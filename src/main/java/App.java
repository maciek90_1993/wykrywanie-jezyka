import com.google.common.base.Optional;
import com.optimaize.langdetect.LanguageDetector;
import com.optimaize.langdetect.LanguageDetectorBuilder;
import com.optimaize.langdetect.i18n.LdLocale;
import com.optimaize.langdetect.ngram.NgramExtractors;
import com.optimaize.langdetect.profiles.LanguageProfile;
import com.optimaize.langdetect.profiles.LanguageProfileBuilder;
import com.optimaize.langdetect.profiles.LanguageProfileReader;
import com.optimaize.langdetect.profiles.LanguageProfileWriter;
import com.optimaize.langdetect.text.CommonTextObjectFactories;
import com.optimaize.langdetect.text.TextObject;
import com.optimaize.langdetect.text.TextObjectFactory;
import model.Audiobook;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class App {

    public static void main(String[] args) throws IOException {
        List<Audiobook>audiobooks = new ArrayList<Audiobook>();
        AudiobookService audiobookService = new AudiobookService();
        audiobooks = audiobookService.getAudiobooks_list();

        FileWriter fstream = new FileWriter("list_products_with_detected_language.txt");
        fstream.flush();
        BufferedWriter output = new BufferedWriter(fstream);
        output.write("ProductID;languages"+"\n");


        List<LanguageProfile> languageProfiles = new LanguageProfileReader().readAllBuiltIn();
        LanguageDetector languageDetector = LanguageDetectorBuilder.create(NgramExtractors.standard())
                .withProfiles(languageProfiles)
                .build();
        TextObjectFactory textObjectFactory = CommonTextObjectFactories.forDetectingOnLargeText();
        for (Audiobook audiobook:audiobooks) {
            String tmp="";
            tmp = audiobook.getDisplatName()+ " " +audiobook.getDescription()+" "+ audiobook.getOtherText();
            System.out.println(audiobook.getProductID()+"; "+tmp);
            tmp.replace("♫ ♫"," ");
            tmp.replace("\n"," ");
            tmp.replace("\""," ");
            tmp.replace("<p>", " ");
            tmp.replace("<b>", " ");
            tmp.replace("<i>", " ");
            tmp.replace("</p>", " ");
            tmp.replace("</b>", " ");
            tmp.replace("</i>", " ");


            TextObject textObject = textObjectFactory.forText(tmp);
            Optional<LdLocale> lang = languageDetector.detect(textObject);
            try {
                output.write(audiobook.getProductID() + ";" + lang.get().getLanguage() + "\n");
                output.flush();
            } catch(IllegalStateException e) {
                output.write(audiobook.getProductID() + ";" + "Brak rozpoznania" + "\n");
            }

        }

    }



}
