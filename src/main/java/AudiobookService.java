import model.Audiobook;
import model.Audiobook;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AudiobookService {

    private File data;
    private FileInputStream file_input;
    private POIFSFileSystem fs;
    private HSSFWorkbook wb;
    private HSSFSheet sheet;
    private HSSFRow row;
    private HSSFCell cell;
    private int cols;
    private int tmp;
    private int rows;
    private List<Audiobook> audiobooks_list;

    public AudiobookService(){

        this.cols = 0;
        this.tmp = 0;
        this.audiobooks_list = new ArrayList<Audiobook>();
        // this.data = new File(System.getProperty("user.dir")+"\\src\\main\\resources\\data.xls");
        this.data = new File(System.getProperty("user.dir")+"\\src\\main\\resources\\Baza_ES_poprawa_jezyka.xls");
        // this.data = new File(System.getProperty("user.dir")+"\\src\\main\\resources\\data1.xls");
        try {
            this.file_input = new FileInputStream(data);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    private void creatAudiobooksList() throws IOException {
        String[] tab = new String[20];
        int k = 0;
        this.fs = new POIFSFileSystem(this.file_input);
        this.wb = new HSSFWorkbook(this.fs);
        this.sheet = wb.getSheetAt(0);
        this.rows = sheet.getPhysicalNumberOfRows();

        for (int i = 0; i < 10 || i < this.rows; i++) {
            this.row = this.sheet.getRow(i);
            if (this.row != null) {
                this.tmp = this.sheet.getRow(i).getPhysicalNumberOfCells();
                if (this.tmp > this.cols) this.cols = this.tmp;
            }
        }

        for (int r = 1; r < this.rows; r++) {
            this.row = this.sheet.getRow(r);
            if (this.row != null) {
                System.out.print("\n");
                k=0;
                for (int c = 0; c < this.cols; c++) {
                    this.cell = row.getCell((short) c);
                    if (cell != null) {
                        tab[k++] = cell.toString();
                        System.out.print(cell+" \t");
                    }
                }
                this.audiobooks_list.add(new Audiobook(tab));
            }
        }

    }


    public List<Audiobook> getAudiobooks_list() throws IOException {
        this.creatAudiobooksList();
        //this.creatOnlyAudiobookWithID();
        return this.audiobooks_list;
    }

    private void creatOnlyAudiobookWithID() throws IOException {
        String[] tab = new String[20];
        int k = 0;
        this.fs = new POIFSFileSystem(this.file_input);
        this.wb = new HSSFWorkbook(this.fs);
        this.sheet = wb.getSheetAt(0);
        this.rows = sheet.getPhysicalNumberOfRows();

        for (int i = 0; i < 10 || i < this.rows; i++) {
            this.row = this.sheet.getRow(i);
            if (this.row != null) {
                this.tmp = this.sheet.getRow(i).getPhysicalNumberOfCells();
                if (this.tmp > this.cols) this.cols = this.tmp;
            }
        }

        for (int r = 1; r < this.rows; r++) {
            this.row = this.sheet.getRow(r);
            if (this.row != null) {
                System.out.print("\n");
                k=0;
                for (int c = 0; c < this.cols; c++) {
                    this.cell = row.getCell((short) c);
                    if (cell != null) {
                        tab[k++] = cell.toString();
                        System.out.print(cell+" \t");
                    }
                }
                Audiobook tmp = new Audiobook();
                tmp.setProductID(tab[0]);
                this.audiobooks_list.add(tmp);
            }
        }

    }
}
