package model;

public class Audiobook {

    private String productID;
    private String displayName;
    private String language;
    private String description;
    private String otherText;

    public Audiobook(){ }

    public Audiobook(String[] tab){
        this.productID = tab[0];
        this.displayName = tab[1];
        this.language = tab[2];
        this.description = tab[3];
        this.otherText = tab[4];


    }

    public String getProductID() {
        return productID;
    }

    public void setProductID(String productID) {
        this.productID = productID;
    }

    public String getDisplatName() {
        return displayName;
    }

    public void setDisplatName(String displatName) {
        this.displayName = displatName;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOtherText() {
        return otherText;
    }

    public void setOtherText(String otherText) {
        this.otherText = otherText;
    }
}
